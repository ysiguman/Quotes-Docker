# Docker prod
- docker-compose build
- docker-compose run web rake db:create db:migrate


# TO-DO:
	- [x] Suppression de NPM 
	- [x] Recherche et filtres via React
	- [x] Affichage des citations via Masonry et React
	- [x] Utilisation complète de React
	- [x] Modifier les citations
	- [ ] Etudier nv design
	- [ ] !! Barre de recherche dans header
	- [x] Filtrer via tags
	- [x] Menu Hamburger Material
	- [x] Gestion Multi-tags
	- [x] Tags menu integrate with React Component
	- [x] Migration to Progresql for Heroku
	- [ ] Rendre responsive
	- [ ] Ajouter pagination
	- [ ] Ajouter Favoris

---

# Historique:
	- Install gem js-route to have rails route in js

---

# Planning App (https://www.youtube.com/watch?v=cJxp_O5azc4)
	1. Answer Question
		- What are we building ?
		- Who are we building for ?
		- What features do we need to have ?
	2. Users Stories
	3. Model Our data
	4. Tink though the pages we need in our app

## Questions
	1. We are building an app where we can store and share quotes we like, 
	2. We are building this app for people who like quotes, want to discover quotes and share some with others
	3. What features we want ?
		- Quotes
			- CRUD
			- Tags
			- Like
			- Share
			- Description
		- Tags
			- CRUD

## Users Stories
	- As a user i want to be able to create / edit quotes
	- As a user i want to store quotes in tags it's help to organise what we share
	- As a user i want to be able to likes tags that I / other users share
	
## DATA Model
** Quote **
	- quote_id:int pk
	- author:string
	- text:string

** Tag **
	- tag_id:int pk
	- tagName:string
les favoris sont representé par un tag spécial: favoris

** quote_tag **
	- quote_tag_id:int pk
	- quote_id:int fk
	- tag_id:int fk

## Think though the pages we need in our app
	- home
	- Quotes#index
	- Quotes#show
	- Tags
	- User#profile
