class TagsToQuotes < ActiveRecord::Migration
  def change
	create_table :tags2quotes
    add_column :tags2quotes, :tag_id, :integer
	add_index :tags2quotes, :tag_id
    add_column :tags2quotes, :quote_id, :integer
	add_index :tags2quotes, :quote_id
  end
end
