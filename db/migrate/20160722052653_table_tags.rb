class TableTags < ActiveRecord::Migration
  def change
    create_table :tags
    add_column :tags, :name, :string
  end
end
