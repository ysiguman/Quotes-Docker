FROM ruby:2.3.3

RUN apt-get update -qq && apt-get install -y build-essential

# for postgres
RUN apt-get install -y libpq-dev

# for a JS runtime
RUN apt-get install -y nodejs

ENV APP_HOME /myapp
RUN mkdir $APP_HOME
WORKDIR $APP_HOME

COPY Gemfile Gemfile
RUN gem install nokogiri -v '1.6.8.1'
COPY Gemfile.lock Gemfile.lock
RUN bundle install

ADD . $APP_HOME
