@Quotes = React.createClass
  getInitialState: ->
    quotes: @props.data
    tags: @props.tags
    tags2quote: @props.jonction
    filter: ""
    nameFilter: true
    quoteFilter: true
    tagsFilter: true
    grid: ""

  componentDidUpdate: ->
    @state.grid.masonry('reload')

  componentDidMount: () ->
    console.log 'component Did Mount'
    @state.grid = $("#masonry-container")
    @state.grid.masonry
      itemSelector: ".card"
      columnWidth: 330
      gutterWidth: 20
      isFitWidth: true
      transitionDuration: '2s'

  componentWillMount: () ->
    console.log 'component Will Mount'

  getDefaultProps: ->
    quotes: []

  addQuote: (tab) ->
    tags2quote = tab[2]
    tags = tab[1]
    quote = tab[0]
    newQuotes = @state.quotes.slice()
    newQuotes.push quote
    @setState (quotes: newQuotes, tags: tags, tags2quote: tags2quote)

  deleteQuote: (quote, tab) ->
    tags = tab[0]
    tags2quote = tab[1]
    newQuotes = @state.quotes.slice()
    index = newQuotes.indexOf quote
    newQuotes.splice index, 1
    @setState (quotes: newQuotes, tags: tags, tags2quote: tags2quote)

  changeFilter: (query) ->
    @setState filter: query

  changeFilterElem: (nameFilter, quoteFilter, tagsFilter) ->
    @setState (nameFilter: nameFilter, quoteFilter: quoteFilter, tagsFilter: tagsFilter)
    
  searchInArray: (array, query) ->
    i = 0
    for el in array
      if el.indexOf(query) > -1
        i += 1
    return i

  tagFilter: (e) ->
    @setState filter: e

  render: ->
    React.DOM.div null,
      React.DOM.div
        className: "SearchFilter"
        React.createElement SearchBar, filter: @state.filter, handleChangeFilter: @changeFilter, handleElemFilter: @changeFilterElem, authorCheck: @state.nameFilter, quoteCheck: @state.quoteFilter, tagsCheck: @state.tagsFilter
      React.DOM.div
        className: "row"
        React.DOM.div
          className: "col s2"
          React.createElement Tags, tags: @state.tags, tagFilter: @tagFilter, jonction: @state.tags2quote
        React.DOM.div
          className: "col s10"
          React.DOM.div
            className: 'masonry'
            id: 'masonry-container'
            for quote in @state.quotes
              if((quote.author.indexOf(@state.filter) > -1 && @state.nameFilter) || (quote.text.indexOf(@state.filter) > -1 && @state.quoteFilter) || (@searchInArray(quote.tags, @state.filter) >= 1 && @state.tagsFilter))
                React.createElement Quote, key: quote.id, quote: quote, handleDeleteQuote: @deleteQuote
      React.createElement QuotesForm, handleNewQuote: @addQuote
