@QuotesForm = React.createClass
  getInitialState: ->
    author: ''
    text: ''
    tags: ''

  close: ->
    window.closeForm()

  openForm: ->
    window.openForm()

  handleChange: (e) ->
    name = e.target.name
    @setState "#{ name }": e.target.value

  handleSubmit: (e) ->
    e.preventDefault()
    console.log("Send...")
    $.post('/quotes', {quote: @state}, (data) =>
      @close()
      @props.handleNewQuote data
    ).fail( (e) ->
      console.log e
    )

  render: ->
    React.DOM.div
      className: "formContainer"
      React.DOM.div
        className: "btn-mt"
        id: "btn"
        React.DOM.i
          className: "material-icons icon"
          onClick: @openForm
          id: "addBtn"
          "add"
        React.DOM.i
          className: "material-icons icon"
          onClick: @handleSubmit
          id: "sendBtn"
          "send"
      React.DOM.form
        id: "form"
        className: "form"
        React.DOM.div
          id: "close"
          onClick: @close
          React.DOM.i
            className: "material-icons icon2"
            "add"
        React.DOM.div
          className: "formContent"
          React.DOM.div
            className: "row"
            React.DOM.div
              className: "col s12"
              React.DOM.div
                className: "row"
                React.DOM.div
                  className: "input-field col s12"
                  React.DOM.i
                    className: "material-icons prefix"
                    "account_circle"
                  React.DOM.input
                    type: "text"
                    value: @props.author
                    id: "author"
                    name: "author"
                    onChange: @handleChange
                    className: "validate"
                  React.DOM.label
                    htmlFor: "author"
                    "author"
          React.DOM.div
            className: "row"
            React.DOM.div
              className: "col s12"
              React.DOM.div
                className: "row"
                React.DOM.div
                  className: "input-field col s12"
                  React.DOM.i
                    className: "material-icons prefix"
                    "format_quote"
                  React.DOM.textarea
                    id: "text"
                    name: "text"
                    onChange: @handleChange
                    className: "materialize-textarea text"
                  React.DOM.label
                    htmlFor: "text"
                    "quote"
          React.DOM.div
            className: "row"
            React.DOM.div
              className: "col s12"
              React.DOM.div
                className: "row"
                React.DOM.div
                  className: "input-field col s12"
                  React.DOM.i
                    className: "material-icons prefix"
                    "local_offer"
                  React.DOM.input
                    type: "text"
                    id: "tags"
                    name: "tags"
                    onChange: @handleChange
                    className: "validate"
                  React.DOM.label
                    htmlFor: "tags"
                    "tags"
