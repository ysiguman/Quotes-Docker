@SearchBar = React.createClass
  handleChange: (e) ->
    query = e.target.value
    @props.filter = query
    @props.handleChangeFilter query

  handleElemFilter: (e) ->
    nameFilter = $("#Name").is(':checked')
    tagsFilter = $("#Tags").is(':checked')
    quoteFilter = $("#Quote").is(':checked')
    @props.handleElemFilter nameFilter, quoteFilter, tagsFilter
    
  render: ->
    React.DOM.div
      className: "SearchDiv"
      React.DOM.form
        id: "searchForm"
        React.DOM.input
          type: "text"
          name: "tags"
          id:"search"
          value: @props.filter
          placeholder: "Search ..."
          onChange: @handleChange
        React.DOM.div
          className: "row filter"
          React.DOM.div
            className: "col s1"
            React.DOM.p null,
              React.DOM.p
                "Filter by: "
          React.DOM.div
            className: "col s1"
            React.DOM.p null,
              React.DOM.input
                type: "checkbox"
                value: @props.authorCheck
                checked: @props.authorCheck
                name: "Name"
                id: "Name"
                onChange: @handleElemFilter
              React.DOM.label
                htmlFor: "Name"
                "Name"
          React.DOM.div
            className: "col s1"
            React.DOM.p null,
              React.DOM.input
                type: "checkbox"
                value: @props.quoteFilter
                checked: @props.quoteCheck
                name: "Quote"
                id: "Quote"
                onChange: @handleElemFilter
              React.DOM.label
                htmlFor: "Quote"
                "Quote"
          React.DOM.div
            className: "col s1"
            React.DOM.p null,
              React.DOM.input
                type: "checkbox"
                value: "Tags"
                name: @props.tagsCheck
                checked: @props.tagsCheck
                id: "Tags"
                onChange: @handleElemFilter
              React.DOM.label
                htmlFor: "Tags"
                "Tags"
