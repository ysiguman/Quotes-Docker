@Tags = React.createClass
  setTag: (e) ->
    @props.tagFilter e

  countTags: (id) ->
    it = 0
    for e in @props.jonction
      if e.tag_id == id
        it += 1
    return it

  render: ->
    React.DOM.div
      className: "collection"
      React.DOM.p
        className: "collection-item"
        "Tags :"
      React.DOM.a
        className: "collection-item"
        href: "/#"
        onClick: =>  @setTag ""
        "No Tag Filter"
      for tag in @props.tags
        React.createElement Tag, key: tag.id, name: tag.name, setTag: @setTag, numberTags: @countTags tag.id

@Tag = React.createClass
  handleSetTag: () ->
    @props.setTag @props.name

  render: ->
    React.DOM.a
      className: "collection-item"
      onClick: @handleSetTag
      href: "/#"
      @props.name
      React.DOM.span
        className: "badge"
        @props.numberTags
