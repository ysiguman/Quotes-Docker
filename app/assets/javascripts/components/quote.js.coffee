@Quote = React.createClass
  handleDelete: () ->
    $.ajax
      method: 'DELETE'
      url: "/quotes/#{ @props.quote.id }"
      success: (e) =>
        @props.handleDeleteQuote @props.quote, e
      error: (e) =>
        console.log('Error:')
        console.log(e)
  render: ->
    React.DOM.div
      className: 'card'
      id: "card-#{ @props.quote.id }"
      React.DOM.div
        className: 'card-content'
        React.DOM.span
          className: 'card-title'
          @props.quote.author
        React.DOM.p
          className: null
          @props.quote.text
      React.DOM.div
        className: "tags"
        for tag, i in @props.quote.tags
          React.DOM.p
            key: i
            className: "chip"
            tag
      React.DOM.div
        className: 'card-action'
        React.DOM.a
          className: null
          href: Routes.edit_quote_path( @props.quote.id).toString()
          'Edit'
        React.DOM.a
          className: null
          onClick: @handleDelete
          'Delete'
