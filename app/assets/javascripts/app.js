//Materialize function:
$(document).ready(function() {
	console.log("hello")
	console.log('v1.0.2')
	Materialize.updateTextFields();
	$(".button-collapse").sideNav();
});
$("#search").siblings('label, .prefix').addClass('active');


function openForm()
{
	console.log("Open form")
	$("#btn").css(
	{
		"right": "50%",
		"transform": "translateX(50%)",
		"background-color": "#4db6ac"
	});

	setTimeout(function () 
	{
		$("#addBtn").hide();
		$("#sendBtn").show();
		$(".form").css({
			"width": "350px",
			"height": "450px"
		});
	}, 400);
};

function closeForm()
{
	console.log("Close Form");
	$("#close .icon2").css(
		"transform", "rotate(45deg)"
	);

	$("#addBtn").show();
	$("#sendBtn").hide();
	$(".form").css(
	{
		"width": "0px",
		"height": "0px"
	});

	setTimeout(function () 
	{
		$("#btn").css(
		{
			"width": "55.5px",
			"height": "55.5px",
			"border-radius": "50%",
			"background-color": "#F44336",
			"cursor": "pointer",
			"right": "50px",
			"transform": "translateX(0%)"
		});
	}, 500);
}
