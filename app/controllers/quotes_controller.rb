class QuotesController < ApplicationController
	def index
		# @quotes = Quote.all
		@tags = Tag.all
		@jonction = Tags2quote.all
		@quotes = []
		Quote.all.each_index do |i|
			@quotes[i] = QuoteTags.new
			@quotes[i].id = Quote.all[i].id
			@quotes[i].author = Quote.all[i].author
			@quotes[i].text = Quote.all[i].text
			@quotes[i].tags = getTags(@quotes[i].id, @jonction, @tags)
		end

	end	

	def show
		@quote = Quote.find(params[:id])
	end

	def new
		@quote = Quote.new
	end
	
	def edit
		@quote = Quote.find(params[:id])
	end
	def update
		@quote = Quote.find(params[:id])
		
		if @quote.update(quote_params)
			redirect_to quotes_path
		else
			render 'edit'
		end
	end

	def create
		@params 	= 	quote_params
		@tagsTab 	= 	@params["tags"].split(' ')

		# Create Quote
		@quote = Quote.new
		@quote.author 	= 	@params["author"]
		@quote.text 	= 	@params["text"]
		@quote.save
		@quoteId = Quote.last.id

		if @tagsTab.length != 0 then
			@tagsTab.each do |newTag|
				#Create Tag
				@tags 	= Tag.all.where("name = '#{newTag}'")
				if @tags.length == 0 
					@tag = Tag.new
					@tag["name"] 	= 	newTag
					@tag.save
				end
				@tagId = Tag.all.where("name =  '#{newTag}'")[0].id

				#Create Association
				@tag2quote 	= Tags2quote.new
				@tag2quote.tag_id = @tagId
				@tag2quote.quote_id = @quoteId
				@tag2quote.save
			end
		end

		#redirect_to @quote
		if @quote.save
			@tags = Tag.all
			@jonction = Tags2quote.all
			
			@tab = []
			@send = QuoteTags.new
			@send.id = Quote.last.id
			@send.author = @quote.author
			@send.text = @quote.text
			@send.tags = getTags(@send.id, @jonction, @tags)
			
			@tab.push(@send)
			@tab.push(Tag.all)
			@tab.push(Tags2quote.all)

			render json: @tab
		else
			render json: @quote.errors, status: :unprocessable_entity
        end
	end
	
	def destroy
		id = params[:id]
		@quote = Quote.find(id)
		@quote.destroy

		#Remove Association
		@jonction = Tags2quote.where(quote_id: id)
		if @jonction != nil
			@jonction.each do |e|
				tagId = e.tag_id
				e.destroy
				
				tagNumber = 0
				Tags2quote.all.each do |tag|
					if tag.tag_id == tagId
						tagNumber += 1
					end
				end
				if tagNumber === 0
					Tag.find(tagId).destroy
				end
			end
		end
		

		#Remove null tags
		@tab = []
		@tab.push(Tag.all)
		@tab.push(Tags2quote.all)

		render json: @tab
        
		
	end
	
	def random
		@random = Quote.all[rand(Quote.all.length)]
		redirect_to quote_path(@random.id)
	end

	private
		def quote_params
			params.require(:quote).permit(:author, :text, :tags)
		end

		def getTags(id, tags, tagsName)
			tagsTab = []
			tags.each do |tag|
				if tag.quote_id == id
					tagsTab.push(tagsName.find(tag.tag_id).name)
				end
			end
			return tagsTab
		end

		class QuoteTags
			attr_accessor :id, :author, :text, :tags
			def initialize
				@id, @author, @text, @tags = ""
			end 
		end
end
