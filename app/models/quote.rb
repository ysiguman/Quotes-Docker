class Quote < ActiveRecord::Base
	validates :author,	presence: true,
						length: {minimum: 1}
end
